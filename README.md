![visitors](https://visitor-badge.glitch.me/badge?page_id=BigoudOps.readme)

# this is a set of scripts for nautilus.

be sure to have nautilus-scripts-manager installed on your computer before installing this repo.

For Ubuntu you must install this package ➡ <apt://nautilus-scripts-manager>

 After that you have a new folder created at this location /home/user/.local/share/nautilus/scripts you can then add as many scripts as you want.

## In this repo you have:
1. addRhythmbox.sh add selected file (s) to Rhythmbox playlist
2. Create-tar.gz.sh and do what its name suggests, the selected folder or files will be compressed in tar.gz format
3. InstallAPPIMAGE.sh it adds the execution right to the selected APPIMAGE file and adds the shortcut to the applications menu
4. add-to-vlc.sh add selected file (s) to VLC playlist (check that the single instance option is checked ✅)
