#!/bin/sh
base="$(echo "$NAUTILUS_SCRIPT_CURRENT_URI" | cut -d'/' -f3- | sed 's/%20/ /g')"
 
if [ -z "$NAUTILUS_SCRIPT_SELECTED_FILE_PATHS" ]; then
   dir="$base"
else
   while [ ! -z "$1" -a ! -d "$base/$1" ]; do shift; done
   dir="$base/$1"
fi
 
if [ "$NAUTILUS_SCRIPT_CURRENT_URI" = "x-nautilus-desktop:///" ]; then
dir="Desktop"
fi
 
if [ "$NAUTILUS_SCRIPT_CURRENT_URI" = "trash:" ]; then
dir="$HOME/.Trash"
fi
 
if [ "$NAUTILUS_SCRIPT_CURRENT_URI" = "file:///" ]; then
dir="/"
fi
 
FIRST_URI="$(echo -n "$NAUTILUS_SCRIPT_SELECTED_URIS")"
if [ "$FIRST_URI" = "x-nautilus-desktop:///home" ]; then
dir="$HOME"
fi
 
if [ "$FIRST_URI" = "x-nautilus-desktop:///computer" ]; then
dir="/"
fi
rhythmbox "$dir"
